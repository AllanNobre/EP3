- Universidade de Brasília - Faculdade do Gama
- Disciplina de Orientação a Objetos
- Professor: Renato Coral Sampaio
- Alunos: Allan Jefrey Pereira Nobre  -   15/0029624
 		  Hugo Alves dos Santos Barbosa - 15/0036884 
- Nome do site: PercentOff - Sales


--------------------------------------------------------------------------------------
		         Exercício de Programação 3 - Ruby on Rails
--------------------------------------------------------------------------------------


 -> Tema e ideia:
 	Escolhemos o tema de um site de compartilhamento de promoções, ou seja, 
caso alguém tenha visto um produto e/ou serviço em um site ou loja com um preço 
abaixo da média ela pode postá-la, além de também poder ver promoções que outras 
pessoas postaram. Nossa ideia é de que um usuário não cadastrado pode somente ver 
as postagens de outras pessoas, porém o usuário cadastrado, além de poder ver as
promoções, também pode postar e comentar outros posts.
	É importante ressaltar que esse site não compara preços (como sites igual o 
Buscapé e o Zoom) e também não é responsável pela venda de nenhum produto, ou seja, 
é basicamente uma comunidade que compartilha promoções e tem um funcionamento pare-
cido com o de um fórum.
	Uma das ideias iniciais era de criar um sistema em que os usuários poderiam ava-
liar as promoções com estrelas, onde as melhores promoções teriam mais estrelas, dando
uma ideia se esse promoção foi interessante para outros usuários ou não. Pretendíamos
também criar uma página para usuários e outra para gerenciamento de contas para o usu-
ário administrador, além de uma lista de favoritos para armazenar as promoções. Essas
funcionalidades não puderam ser adicionadas devido a alguns contratempos, principal-
mente a falta de tempo para aprendermos mais sobre a linguagem.

____________________________________________________________________________________


-> Funcionalidades e como navegar pelo site:
	O funcionamento do nosso site funciona de forma análoga a de um fórum, ou seja,
um usuário cadastrado tem mais privilégios que um usuário não cadastrado. O usuário
cadastrado, quando logado, pode criar uma promoção, onde ele terá uma opção de colo-
car um título, fazer um breve resumo sobre o produto e a promoção em questão e colo-
car uma imagem do produto, além de poder fazer comentários nos posts de outros membros.
Todo post e comentário que esse usuário criar poderão ser excluídos ou editados a qual-
quer momento. O usuário não cadastrado não possui esses privilégios, por isso ele só
consegue ver os posts que os usuários cadastrados criaram.

 - Como ver uma promoção:
 	Qualquer usuário pode ver qualquer promoção postada no site e elas aparecem lis-
 tadas na página inicial. Para ter mais informações sobre alguma promoção, como ler
 a descrição (onde se encontra o link ou endereço da loja) e comentários o usuário 
 pode tanto clicar na imagem que ilustra o post quanto no botão "Read more...". Assim
 ele será redirecionado para a página que abriga o post inteiro.

 - Como criar uma promoção:
 	Para compartilhar uma promoção, o usuário deve clicar no botão "New sale" (esse botão
 só aparece para usuários logados). Esse botão redireciona o usuário para outra página
 que contem três campos: o primeiro é o campo de título e é onde o usuário deve digitar
 o nome da loja, do produto e o preço, a fim de chamar a atenção de outros usuários, sendo
 esse texto bem curto. O segundo campo é onde o usuário descreve com mais detalhes sobre o
 produto e a promoção, sendo que aqui que ele deve colocar o link ou o endereço da loja 
 que possui o desconto. O terceiro campo deve ser preenchido com o link de uma imagem 
 do produto ou que ilustre a promoção. Note que todos esses campos devem obrigatoriamente
 ser preenchidos, caso contrário o site não deixa o usuário criar o post e as alterações só
 serão mostradas para os outros usuários depois de apertar o botão "New sale".

 -Como criar um comentário:
 	Para comentar uma promoção o usuário deve estar logado e entrar na página do post.
 No final da tela se encontra um campo de texto editável e é onde o usuário deverá digitar 
 seu texto. Após digitá-lo, o comentário só irá aparecer para os outros usuários ao 
 apertar no botão "Create comment"

 - Como se cadastrar, entrar e sair:
 	Para se cadastrar, o usuário deve clicar no botão "Join us", localizado no canto
 superior direito da tela e preencher os campos de email e senha, que devem conter no
 mínimo 6 caracteres.
 	Após efetuado o cadastro, o usuário automaticamente já estará logado, porém, caso
 ele saia e não esteja mais logado, ele deve clicar no botão "Log in", também localiza-
 do no canto superior direito da tela, e preencher os campos com seu email e senha ca-
 dastrados. Caso ele queira encerrar sua sessão o usuário deve clicar no botão "Log out".

- Editando sua conta:
	Para saber se você está logado no site, basta dar uma olhada para a barra superior do
site, pois sempre que um usuário está logado o seu email cadastrado fica visível nela.
Sabendo disso, caso um usuário queira fazer modificações em sua conta ele pode, basta que
ele clique no seu email.
	Ao clicar no email, o usuário é direcionado para outra página, onde ele pode mudar sua
senha, mudar seu email e deletar toda sua conta. Ao deletar uma conta, o usuário não conse-
gue mais entrar no sistema com o email que estava cadastrado, sendo que ele deve se cadastrar
novamente para poder ter todos os privilégios que ele tinha. Além disso, ao deletar uma conta,
todos os posts e comentários associados a essa conta também são deletados.

- Usuário Administrador:
	O usuário administrador é um usuário especial para o sistema, pois ele tem permissões para
fazer o que quiser em todo o site. Esse atributo de administrador é tão forte que um usuário só
pode ganhá-lo por meio do console do Rails. O administrador, além de ter todos os privilégios de
um usuário cadastrado comum, também pode editar e deletar tudo de qualquer outro usuário, ou 
seja, o administrador pode moderar tudo o que se passa no site, desde comentários até mesmo 
outros usuários. 

- Sobre nós:
	Outro botão localizado na parte superior do site, que aparece para qualquer usuário é o
"About us". Ao clicar nesse botão o usuário é redirecionado para uma página que conta um
pouco mais sobre o site e seus desenvolvedores, caso o usuário queira saber um pouco mais
sobre nós. Também é possível acessar essa página clicando em nossa logo que se encontra no
final da página. 

____________________________________________________________________________________


-> Atributos e métodos disponíveis:
- Na classe "Sale":
	A classe "Sale" é a classe mais importante, pois é ela que molda o ideia do nosso site,
ou seja, é com essa classe que mexemos com tudo que se refere as promoções. Ela recebe como
atributos duas Strings, chamadas 'title' e 'image_path' e um Text chamado 'content'. O 'title'
é o título do post, o 'image_path' é o caminho da imagem que ilustra o post e o 'content' é
onde fica a descrição da promoção.
	Essa classe "Sale" tem uma associação de pertencer a classe "User" (belongs_to) e de ter
muitas "Comment" (has_many), ou seja, ela pode ter muitos comentários e só poderá ter um usu-
ário associado. Além disso, ela também tem elementos de CRUD, pois um usuário pode criar, ler,
editar e deletar posts (que nesse caso são "Sales").
	O método 'index' mostra a página principal do site, que serão as postagens ordenadas crono-
logicamente. O método 'show' mostra a promoção que foi criada e o método 'new' será responsável
por abrir a página onde o post será criado, enquanto o 'create' cria de fato o post. O método
'edit' irá criar a página onde a promoção poderá ser editada, enquanto 'update' irá atualizar
a promoção após as devidas atualizações. O método 'destroy' destroi o post e redireciona o usu-
ário para a página inicial, o 'find_sale' encontra a promoção pelo ID correto e por fim 'sale_params'
pega os parâmetros e salva.

- Na classe "Comment":
	A classe "Comment" é usada para que o usuário possa criar comentários dentro dos posts e 
tem somente um atributo, que é um Text chamado de 'comment' e esse atributo será justamente o
comentário.
	Essa classe tem uma associação de pertencer ao "User" e a "Sale" (belongs_to), ou seja, 
um comentário só terá um usuário e um post associado a ele. O comentário também possui ele-
mentos de CRUD muito semelhantes com o da classe "Sale".
	O método 'create' cria do comentário, relacionando o ID do usuário com ele. O método
'edit' irá criar a página onde o comentário poderá ser editada, enquanto 'update' irá atualizar
o comentário após as devidas atualizações. O método 'destroy' destroi o comentário e redireciona
o usuário para a o post.


- Na classe "User":
	A classe "User" também é de suma importância para a aplicação, pois é com ela que serão
definidos os tipos de usuários (cadastrados ou não), além de associar posts e comentários. Seus
atributos são duas Strings, uma chamada de 'email' e a outra de 'password' e um Boolean chamado
de 'admin'. O 'email' e 'password' serão o email e a senha que o usuário cadastra, respectivamente,
e o 'admin' serve para separar usuários administradores (igual a true) e usuários comum (igual a
false).
	Essa classe tem uma relação de ter várias "Sales" e "Comments" (has_many), ou seja, um único 
usuário pode ter várias "Sales" e vários "Comments" associados a ele. Aqui também pode-se observar
os elementos do CRUD nitidamente, pois um usuário pode criar, editar e deletar contas, posts e 
comentários, além de poder le-los.
	Os métodos da classe "User" foram criados pela Gem Devise e por isso os métodos dessa classe
são os métodos default da Gem.

- Gems:
	Utilizamos as gems default do Rails e adquirimos somente o Devise, para criar usuários e gerar
permissões.