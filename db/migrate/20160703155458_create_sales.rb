class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.string :title
      t.text :content
      t.string :image_path

      t.timestamps null: false
    end
  end
end
