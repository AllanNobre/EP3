class Sale < ActiveRecord::Base
	belongs_to :user
	has_many :comments

	validates_presence_of :image_path, :message => "Put some image path!"
	validates_presence_of :title, :message => "Put some title!"
	validates_presence_of :content, :message => "Put some content!"
end
