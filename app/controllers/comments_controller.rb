class CommentsController < ApplicationController
	#Cria o comentário
	def create
		@sale = Sale.find(params[:sale_id])
		@comment = @sale.comments.create(params[:comment].permit(:comment))
		@comment.user_id = current_user.id if current_user
		@comment.save

		if @comment.save
			redirect_to sale_path(@sale)
		else
			render 'new'
		end
	end

	#Edita o comentário
	def edit
		@sale = Sale.find(params[:sale_id])
		@comment = @sale.comments.find(params[:id])
	end

	#Edita o comentário
	def update
		@sale = Sale.find(params[:sale_id])
		@comment = @sale.comments.find(params[:id])
		
		if @comment.update(params[:comment].permit(:comment))
			redirect_to sale_path(@sale)
		else
			render 'edit'
		end
	end

	#Deleta o comentário
	def destroy
		@sale = Sale.find(params[:sale_id])
		@comment = @sale.comments.find(params[:id])
		@comment.destroy
		redirect_to sale_path(@sale)
	end
end