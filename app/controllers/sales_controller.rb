class SalesController < ApplicationController
	before_action :find_sale, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!, except: [:index, :show]

	#Página Principal
	def index
		@sales = Sale.all.order("created_at DESC")
	end

	#Mostra a Promoção
	def show
	end

	#Cria uma nova promoção
	def new
		@sale = current_user.sales.build
	end

	#Cria uma nova promoção
	def create
		@sale = current_user.sales.build(sale_params)

		if @sale.save
			redirect_to @sale
		else
			render 'new'
		end
	end

	#Edita a promoção
	def edit
	end

	#Edita a promoção
	def update
		if @sale.update(sale_params)
			redirect_to @sale
		else
			render 'edit'
		end
	end

	#Deleta a promoção
	def destroy
		@sale.destroy
		redirect_to root_path
	end

	private

	#Encontra a promoção pelo ID correto
	def find_sale
		@sale = Sale.find(params[:id])
	end

	#Puxa os parâmetros do construtor
	def sale_params
		params.require(:sale).permit(:title, :content, :image_path)
	end	
end